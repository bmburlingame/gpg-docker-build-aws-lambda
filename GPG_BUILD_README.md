# Build GPG From Scratch for use in Lambda

Need to build GPG from the source code in Amazon Linux Docker Container to get GPG to work.

Dockerfile to Build is here: `util/gpg/Dockerfile`

###### Execute the following to build a zip file of all GPG build contents
```bash
$ docker build -t <CONTAINER_NAME>/<TAG> .
```

###### Extract the zip file from the container

Run the container
```bash
$ docker run -it <CONTAINER_NAME>/<TAG>
```

Get the container id for your build
```bash
$ docker container ls
```

Extract the contents
```bash
$ docker cp <CONTAINER_ID>:/opt/gpg_build.zip .
```

###### Need to prune the zip file to only the needed files (too big for lambda otherwise)

Run the following commands to create the proper structure needed
```bash
$ unzip gpg_build.zip
$ rm gpg_build.zip
$ cd gpg
$ rm -r include/ libexec/ sbin/ share/ lib/pkgconfig/
$ rm lib/*.so lib/*.la lib/libassuan.so.0.* lib/libgcrypt.so.20.* lib/libgpg-error.so.0.* lib/libksba.so.8.* lib/libnpth.so.0.*
$ mv bin/gpg .
$ rm -r bin
```

###### Add to lambda
When added to lambda, the executable and lib/ folder needs to be at the same level as all the other packages.
Then when linking to the executable, the full path on the lambda box should be `/var/task/gpg`