FROM amd64/amazonlinux:2

# Download and Install Dependencies
RUN yum -y update
RUN yum -y install gcc
RUN yum -y install bzip2
RUN yum -y install wget
RUN yum -y install tar
RUN yum -y install gzip
RUN yum -y install make

WORKDIR /opt
RUN mkdir gpg

RUN wget https://www.gnupg.org/ftp/gcrypt/npth/npth-1.6.tar.bz2
RUN tar xjf npth-1.6.tar.bz2
RUN rm npth-1.6.tar.bz2
WORKDIR npth-1.6
RUN ./configure --prefix=/opt/gpg && make
RUN make install
WORKDIR /opt

RUN wget https://www.gnupg.org/ftp/gcrypt/libgpg-error/libgpg-error-1.47.tar.bz2
RUN tar xjf libgpg-error-1.47.tar.bz2
RUN rm libgpg-error-1.47.tar.bz2
WORKDIR libgpg-error-1.47
RUN ./configure --prefix=/opt/gpg && make
RUN make install
RUN cp /opt/libgpg-error-1.47/src/gpg-error-config /opt/gpg/bin/
WORKDIR /opt

RUN wget https://www.gnupg.org/ftp/gcrypt/libassuan/libassuan-2.5.5.tar.bz2
RUN tar xjf libassuan-2.5.5.tar.bz2
RUN rm libassuan-2.5.5.tar.bz2
WORKDIR libassuan-2.5.5
RUN ./configure --prefix=/opt/gpg --with-libgpg-error-prefix=/opt/gpg && make
RUN make install
WORKDIR /opt

RUN wget https://www.gnupg.org/ftp/gcrypt/libgcrypt/libgcrypt-1.10.2.tar.bz2
RUN tar xjf libgcrypt-1.10.2.tar.bz2
RUN rm libgcrypt-1.10.2.tar.bz2
WORKDIR libgcrypt-1.10.2
RUN ./configure --prefix=/opt/gpg --with-libgpg-error-prefix=/opt/gpg && make
RUN make install
RUN cp /opt/libgcrypt-1.10.2/src/libgcrypt-config /opt/gpg/bin/
RUN cp /opt/libgcrypt-1.10.2/src/gcrypt.h /opt/gpg/include/
WORKDIR /opt

RUN wget https://www.gnupg.org/ftp/gcrypt/libksba/libksba-1.6.3.tar.bz2
RUN tar xjf libksba-1.6.3.tar.bz2
RUN rm libksba-1.6.3.tar.bz2
WORKDIR libksba-1.6.3
RUN ./configure --prefix=/opt/gpg --with-libgpg-error-prefix=/opt/gpg && make
RUN make install
WORKDIR /opt

RUN wget https://www.gnupg.org/ftp/gcrypt/pinentry/pinentry-1.2.1.tar.bz2
RUN tar xjf pinentry-1.2.1.tar.bz2
RUN rm pinentry-1.2.1.tar.bz2
WORKDIR pinentry-1.2.1
RUN ./configure --prefix=/opt/gpg --enable-pinentry-tty --with-libgpg-error-prefix=/opt/gpg --with-libassuan-prefix=/opt/gpg  && make
RUN make install
WORKDIR /opt

RUN export LD_LIBRARY_PATH=/opt/gpg/lib
RUN wget https://www.gnupg.org/ftp/gcrypt/gnupg/gnupg-2.4.1.tar.bz2
RUN tar xjf gnupg-2.4.1.tar.bz2
RUN rm gnupg-2.4.1.tar.bz2
WORKDIR gnupg-2.4.1
RUN ./configure --prefix=/opt/gpg \
  --with-ksba-prefix=/opt/gpg \
  --with-npth-prefix=/opt/gpg \
  --with-libgpg-error-prefix=/opt/gpg \
  --with-libgcrypt-prefix=/opt/gpg \
  --with-libassuan-prefix=/opt/gpg
RUN make
RUN make install
WORKDIR /opt

RUN yum install -y zip
RUN zip -r gpg_build.zip gpg

